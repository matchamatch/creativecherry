<form action="" method="post">
	{{ csrf_field() }}
	<select name="runnerId">
		<option value="disabled" selected disabled>Please choose a runner</option>
	@foreach($aRunners AS $item)
		<option value="{{ $item->runnerId }}">{{ $item->firstName }} {{ $item->lastName }}</option>
	@endforeach
	</select>
	<select name="raceId">
		<option value="disabled" selected disabled>Please choose a race</option>
	@foreach($aRaces AS $item)
		<option value="{{ $item->raceId }}">{{ $item->raceName }}</option>
	@endforeach
	</select>
	<input type="text" name="time" placeholder="Race Time" required>
	<input type="submit" name="enterResultSubmit" value="Enter Result">
</form>
