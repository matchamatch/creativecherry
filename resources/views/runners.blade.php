@include('head')
<button class="options manualRegister">Manual Register</button>
<button class="options viewResult">Find Result</button>
<div class="formArea">
	<div class="third">
	<div class="manualRegister">
		@include('manualRegister')
	</div>
	<div class="viewResult">
		@include('viewResult')
	</div>
</div>
	<div class="third">
		@if (isset($results))
			<table>
				<thead>
					<tr>
						<th>Race Name</th>
						<th>Time</th>
					</tr>
				</thead>
				<tbody>
			@foreach ($results AS $item)
				<tr>
					<td>{{ $item->raceName }}</td>
					<td>{{ $item->time }}</td>
				</tr>
			@endforeach
				</tbody>
			</table>
		@endif
	</div>
</div>