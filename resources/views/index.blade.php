@include('head')
<button class="options manualRegister">Manual Register</button>
<button class="options createRace">Create Race</button>
<button class="options enterResult">Enter Result</button>
<div class="formArea">
	<div class="third">
	<div class="manualRegister">
		@include('manualRegister')
	</div>
	<div class="createRace">
		@include('createRace')
	</div>
	<div class="enterResult">
		@include('enterResult')
	</div>
</div>
</div>