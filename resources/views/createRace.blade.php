<form action="" method="post">
	{{ csrf_field() }}
	<input type="text" name="raceName" placeholder="Race Name" required>
	<input type="text" name="raceLocation" placeholder="Location" required>
	Date: <input type="date" name="date" required>
	<input type="text" name="raceDistance" placeholder="Distance" required>
	<input type="submit" name="createRaceSubmit" value="Create Race">
</form>