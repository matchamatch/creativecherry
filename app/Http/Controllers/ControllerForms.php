<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Http\Requests;
use App\forms;

class ControllerForms extends Controller{
	public function formInsert(Request $request){

		if (Request::has('createRaceSubmit')){
		forms::createRace(Request::all());
		}

		if (Request::has('manualRegisterSubmit')){
		forms::manualRegister(Request::all());
		}

		if (Request::has('enterResultSubmit')){
		forms::enterResult(Request::all());
		}
		return redirect()->to('/');
	}

	public function content(){
		$aRaces = forms::getRaces();
		$aRunners = forms::getRunners();
	    return view('index', compact('aRunners', 'aRaces'));
	}
}
