<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Http\Requests;
use App\runners;

class ControllerRunners extends Controller{
	public function formInsert(Request $request){

		if (Request::has('manualRegisterSubmit')){
		runners::manualRegister(Request::all());
		}

		if (Request::has('viewResultsSubmit')){
		$results = runners::viewResult(Request::all());
		return view('runners', compact('results'));
		}
		return redirect()->to('/runners');
	}

	public function content(){
	    return view('runners');
	}
}