<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class runners extends Model
{
	public static function manualRegister($request){
	    DB::table('runners')->insert([
	    	'firstName' => $request['firstName'],
	    	'lastName' => $request['lastName'],
	    	'email' => $request['email'],
	    	'ageCategory' => $request['ageCategory'],
	    	'password' => hash('sha256', $request['password'])
	    ]);
    }

	public static function viewResult($request){
    	$query = DB::table('results')
    				->where('runnerId', '=', $request['runnerId'])
    				->join('races', 'results.raceId', '=', 'races.raceId')
		            ->select('results.time', 'races.raceName')
    				->get();
    	return $query;
    }

}
