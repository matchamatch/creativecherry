<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class forms extends Model
{
	public static function createRace($request){
	    DB::table('races')->insert([
	    	'raceName' => $request['raceName'],
	    	'raceLocation' => $request['raceLocation'],
	    	'date' => $request['date'],
	    	'distance' => $request['raceDistance']
	    ]);
    }

	public static function manualRegister($request){
	    DB::table('runners')->insert([
	    	'firstName' => $request['firstName'],
	    	'lastName' => $request['lastName'],
	    	'email' => $request['email'],
	    	'ageCategory' => $request['ageCategory'],
	    	'password' => hash('sha256', $request['password'])
	    ]);
    }

	public static function enterResult($request){
	    DB::table('results')->insert([
	    	'raceId' => $request['raceId'],
	    	'runnerId' => $request['runnerId'],
	    	'time' => $request['time']
	    ]);
    }

    public static function getRaces(){
    	$query = DB::table('races')->select('raceId', 'raceName')->get();
    	return $query;
    }

    public static function getRunners(){
    	$query = DB::table('runners')->select('runnerId', 'firstName', 'lastName', 'ageCategory')->get();
    	return $query;
    }

}
