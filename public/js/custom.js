$(document).ready(function(){
	$("div.manualRegister").hide();
	$("div.createRace").hide();
	$("div.enterResult").hide();
	$("div.viewResult").hide();

	$("button.manualRegister").on("click", function(){
		$("div.manualRegister").show();
		$("div.createRace").hide();
		$("div.enterResult").hide();
		$("div.viewResult").hide();
	});

	$("button.createRace").on("click", function(){
		$("div.createRace").show();
		$("div.manualRegister").hide();
		$("div.enterResult").hide();
	});

	$("button.enterResult").on("click", function(){
		$("div.enterResult").show();
		$("div.createRace").hide();
		$("div.manualRegister").hide();
	});

	$("button.viewResult").on("click", function(){
		$("div.viewResult").show();
		$("div.manualRegister").hide();
	});
});